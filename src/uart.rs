use super::MMIO_BASE;
use core::ops;
use gpio;
use volatile_register::*;

use core::fmt;
use spin::Mutex;

const MINI_UART_BASE: u32 = MMIO_BASE + 0x21_5000;

/// Auxilary mini UART registers
#[allow(non_snake_case)]
#[repr(C)]
pub struct RegisterBlock {
    __reserved_0: u32,       // 0x00
    ENABLES: RW<u32>,        // 0x04
    __reserved_1: [u32; 14], // 0x08
    MU_IO: RW<u32>,          // 0x40
    MU_IER: RW<u32>,         // 0x44
    MU_IIR: RW<u32>,         // 0x48
    MU_LCR: RW<u32>,         // 0x4C
    MU_MCR: RW<u32>,         // 0x50
    MU_LSR: RW<u32>,         // 0x54
    MU_MSR: RW<u32>,         // 0x58
    MU_SCRATCH: RW<u32>,     // 0x5C
    MU_CNTL: RW<u32>,        // 0x60
    MU_STAT: RW<u32>,        // 0x64
    MU_BAUD: RW<u32>,        // 0x68
}

pub struct MiniUart;

/// Deref to RegisterBlock
///
/// Allows writing
/// ```
/// self.MU_IER.read()
/// ```
/// instead of something along the lines of
/// ```
/// unsafe { (*MiniUart::ptr()).MU_IER.read() }
/// ```
impl ops::Deref for MiniUart {
    type Target = RegisterBlock;

    fn deref(&self) -> &Self::Target {
        unsafe { &*Self::ptr() }
    }
}

impl MiniUart {
    pub fn new() -> MiniUart {
        MiniUart
    }

    /// Returns a pointer to the register block
    fn ptr() -> *const RegisterBlock {
        MINI_UART_BASE as *const _
    }

    ///Set baud rate and characteristics (115200 8N1) and map to GPIO
    pub fn init(&self) {
        // initialize UART
        unsafe {
            self.ENABLES.modify(|x| x | 1); // enable UART1, AUX mini uart
            self.MU_IER.write(0);
            self.MU_CNTL.write(0);
            self.MU_LCR.write(3); // 8 bits
            self.MU_MCR.write(0);
            self.MU_IER.write(0);
            self.MU_IIR.write(0xC6); // disable interrupts
            self.MU_BAUD.write(270); // 115200 baud

            // map UART1 to GPIO pins
            (*gpio::GPFSEL1).modify(|x| {
                // Modify with a closure
                let mut ret = x;
                ret &= !((7 << 12) | (7 << 15)); // gpio14, gpio15
                ret |= (2 << 12) | (2 << 15); // alt5

                ret
            });

            (*gpio::GPPUD).write(0); // enable pins 14 and 15
            for _ in 0..150 {
                asm!("nop" :::: "volatile");
            }

            (*gpio::GPPUDCLK0).write((1 << 14) | (1 << 15));
            for _ in 0..150 {
                asm!("nop" :::: "volatile");
            }
            (*gpio::GPPUDCLK0).write(0); // flush GPIO setup
            self.MU_CNTL.write(3); // enable Tx, Rx
        }
    }

    /// Send a character
    pub fn send(&self, c: char) {
        // wait until we can send
        loop {
            if (self.MU_LSR.read() & 0x20) == 0x20 {
                break;
            }

            unsafe { asm!("nop" :::: "volatile") };
        }

        // write the character to the buffer
        unsafe { self.MU_IO.write(c as u32) };
    }

    /// Receive a character
    pub fn getc(&self) -> char {
        // wait until something is in the buffer
        loop {
            if (self.MU_LSR.read() & 0x01) == 0x01 {
                break;
            }

            unsafe { asm!("nop" :::: "volatile") };
        }

        // read it and return
        let mut ret = self.MU_IO.read() as u8 as char;

        // convert carrige return to newline
        if ret == '\r' {
            ret = '\n'
        }

        ret
    }

    /// Display a string
    pub fn puts(&self, string: &str) {
        for c in string.chars() {
            // convert newline to carrige return + newline
            if c == '\n' {
                self.send('\r')
            }

            self.send(c);
        }
    }
}

macro_rules! print {
    ($($arg:tt)*) => ($crate::uart::print(format_args!($($arg)*)));
}

macro_rules! println {
    () => (print!("\n"));
    ($fmt:expr) => (print!(concat!($fmt, "\n")));
    ($fmt:expr, $($arg:tt)*) => (print!(concat!($fmt, "\n"), $($arg)*));
}


impl fmt::Write for MiniUart {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        self.puts(s);
        Ok(())
    }
}

lazy_static! {
    pub static ref UART: Mutex<MiniUart> = Mutex::new(MiniUart::new());
}

pub fn print(args: fmt::Arguments) {
    use core::fmt::Write;
    UART.lock().write_fmt(args).unwrap();
}