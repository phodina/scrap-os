#![feature(panic_implementation)]
#![feature(global_asm)]
#![no_std]
#![no_main]
#![feature(asm)]

extern crate r0;
extern crate volatile_register;
extern crate spin;
#[macro_use]
extern crate lazy_static;

use core::panic::PanicInfo;
use core::fmt::Write;

const MMIO_BASE: u32 = 0x3F00_0000;

mod gpio;
#[macro_use]
mod uart;

#[panic_implementation]
#[no_mangle]
pub fn panic(_info: &PanicInfo) -> ! {
    loop {}
}

#[no_mangle]
pub unsafe extern "C" fn _init_rust() -> ! {

	extern "C" {
        // Boundaries of the .bss section
        static mut __bss_start: u32;
        static mut __bss_end: u32;
    }

    // Zeroes the .bss section
    r0::zero_bss(&mut __bss_start, &mut __bss_end);

	kernel_main();

    loop {}
}

fn kernel_main() {

    uart::UART.lock().init();

	//write!(uart::UART, "The numbers are {} and {}", 42, 1.0/3.0).unwrap();
    println!("RPI3 Loves {}, {}%!\n", "Rust", "99.9");


    loop {
        uart::UART.send(UART.getc());
	}
}

global_asm!(include_str!("boot.S"));